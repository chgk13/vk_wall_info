# VK wall info

This flask app allows users to download data from group or user walls at `vk.com`. It contains two sections. At `Home` section one can choose the required data, then one can go to the `Statistics` tab and get a summary of that data.