import base64
import datetime
import numpy as np
import pandas as pd
import requests
import time
from app import app
from config import HOST, PAR
from flask import make_response
from io import BytesIO
from matplotlib import pyplot as plt


def compute_table(params):

    deadline = params['deadline']

    # 113607360 = 01.01.2006, vk.com was launched on 10.10.2006
    if time.time() < deadline.timestamp() or deadline.timestamp() < 113607360:
        return {'error_code': -1,
                'error_msg': '''Please set the time between 1
                                Jan 2006 and current time.'''}

    latency = params['latency'] or 0

    params.update({'offset': 0, 'deadline': -1})
    is_pins = params['pins']
    is_num_of_pins = params['num_of_pins']
    del params['num_of_pins']
    params['pins'] = is_num_of_pins or is_pins
    params.update(PAR)

    try:
        r = requests.get(HOST + 'execute.getWall', params=params)
    except requests.exceptions.ConnectionError:
        return {'error_code': 0, 'error_msg': 'Connection error'}

    iter = 0
    result_df = pd.DataFrame()

    if 'execute_errors' in r.json():
        error = r.json()['execute_errors'][0]
        return error

    while True:

        if 'response' in r.json():
            if len(r.json()['response']) == 0:
                break

            tmp_df = pd.DataFrame()
            tmp_df = pd.concat([tmp_df] + [pd.DataFrame(df)
                                           for df in r.json()['response']],
                               ignore_index=True)
            if 'pins' in tmp_df:
                tmp_df['pins'] = tmp_df['pins'].apply(attachment_handle)

            if is_num_of_pins:
                tmp_df['num_of_pins'] = tmp_df['pins'].apply(
                    lambda x: 0 if x is None else len(x.split('; ')))
                if not is_pins:
                    tmp_df.drop('pins', axis=1, inplace=True)

            tmp_df['date'] = pd.to_datetime(tmp_df['date'], unit='s')

            result_df = pd.concat([result_df, tmp_df], ignore_index=True)

            if result_df['date'][len(result_df)-1] >= deadline:
                iter += 1
                params.update({'offset': iter})
                time.sleep(latency)
                r = requests.get(HOST + 'execute.getWall', params=params)
            else:
                break

        elif 'error' in r.json():
            return r.json()['error']

    # Truncate unwanted dates
    if 'date' in result_df:
        result_df = result_df[result_df['date'] >= deadline]

    result_df.set_index('date', drop=True, inplace=True)

    column_name_map = {'id': 'post_id',
                       'pins': 'attachments',
                       'num_of_pins': 'num_of_attachments'}

    result_df.rename(column_name_map, axis=1, inplace=True)

    return result_df


def attachment_handle(attach_lst):
    res = []
    if attach_lst is None:
        return
    for attach in attach_lst:
        if attach['type'] == 'link':
            res.append(attach['link']['url'])
        elif attach['type'] == 'photos_list':
            res.append(f"photo_list: {attach['photos_list']}")
        else:
            res.append(f"{attach['type']}_id: {attach[attach['type']]['id']}")
    return '; '.join(res)


def serve_csv(dataframe, filename):
    resp = make_response(dataframe.to_csv(index=True))
    resp.headers['Content-Disposition'] = f'attachment; filename={filename}'
    resp.headers['Content-Type'] = 'text/csv'
    return resp


def get_stats(df, params):

    stats = [
        {
            'title': '<h2>Total number of posts:</h2>',
            'content': f'<p>{len(df)}</p>'
        }
    ]

    if params['head']:
        head = df.head().to_html()
        stat = {'title': '<h2>First 5 rows</h2>',
                'content': head}
        stats.append(stat)

    if params['describe_table']:
        describe_table = df.describe().drop('count')

        sum_row = df.sum(numeric_only=True)
        describe_table.loc['sum'] = sum_row

        describe_table = describe_table.drop('post_id', axis=1)

        describe_table = describe_table.applymap(format_number).to_html()

        stat = {'title': '<h2>Data description table</h2>',
                'content': describe_table}
        stats.append(stat)

    if params['plot_type'] == 'None':
        return stats
    elif params['plot_type'] == 'line':
        df = group_df(df, params['time_interval'], params['is_mean'])
        plot = draw_line_plot(df, params)
        stat = {'title': '<h2>Plot</h2>',
                'content': plot}
        stats.append(stat)
    return stats


def draw_line_plot(df, params):
    x = df.index

    if params['num_of_pins'] and 'num_of_attachments' in df.columns:
        plt.plot(x, df['num_of_attachments'], label='Attachments')
    if params['num_of_likes'] and 'num_of_likes' in df.columns:
        plt.plot(x, df['num_of_likes'], label='Likes')
    if params['num_of_reposts'] and 'num_of_reposts' in df.columns:
        plt.plot(x, df['num_of_reposts'], label='Reposts')
    if params['num_of_comments'] and 'num_of_comments' in df.columns:
        plt.plot(x, df['num_of_comments'], label='Comments')

    plt.legend(loc='best', shadow=True, fontsize='small')
    plt.xlabel('Time scale')
    plt.ylabel('N')

    if params['time_interval'] == 'day':
        plt.xticks([0, 1, 2, 3, 4, 5, 6],
                   ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'])
    elif params['time_interval'] == 'month':
        plt.xticks(list(range(1, 13)),
                   ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN',
                    'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'])
    elif params['time_interval'] == 'hour':
        plt.xticks(list(range(0, 24, 3)),
                   [f'{h}:00' for h in range(0, 24, 3)])

    buffer = BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    plot_url = base64.b64encode(buffer.getvalue()).decode()
    plt.close()
    return f'data:image/png;base64,{plot_url}'


def group_df(df, time_interval, is_mean):

    if time_interval == 'year':
        df = df.groupby(df.index.year)
    elif time_interval == 'month':
        df = df.groupby(df.index.month)
    elif time_interval == 'day':
        df = df.groupby(df.index.dayofweek)
    elif time_interval == 'hour':
        df = df.groupby(df.index.hour)

    if is_mean:
        df = df.mean()
    else:
        df = df.sum()

    return df


def format_number(x):
    if x != x:
        return x
    return str(int(x)) if abs(x - int(x)) < 1e-6 else str(round(x, 2))
