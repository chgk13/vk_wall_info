from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, DateTimeField, SelectField,\
    FloatField
from wtforms.validators import InputRequired
import datetime


FORMAT = '%d-%m-%Y %H:%M'


class InputForm(FlaskForm):
    user_id = StringField('user_id', validators=[InputRequired()])
    from_time = DateTimeField(
        'from_fime',
        format=FORMAT,
        default=datetime.datetime.strptime('10-10-2006 00:00', FORMAT)
    )
    post_id = BooleanField('post_id', default=True)
    text = BooleanField('text', default=True)
    pins = BooleanField('pins', default=True)
    num_of_pins = BooleanField('num_of_pins', default=True)
    num_of_likes = BooleanField('num_of_likes', default=True)
    num_of_reposts = BooleanField('num_of_reposts', default=True)
    num_of_comments = BooleanField('num_of_comments', default=True)
    latency = FloatField('latency', default=0.0)


class StatisticForm(FlaskForm):
    head = BooleanField('head', default=True)
    describe_table = BooleanField('describe_table', default=True)
    plot_type = SelectField('plot_type',
                            choices=[(None, 'None'),
                                     ('line', 'Line plot')])
    time_interval = SelectField('time_interval',
                                choices=[('year', 'Year'),
                                         ('month', 'Month'),
                                         ('day', 'Day of week'),
                                         ('hour', 'Hour')])

    is_mean = BooleanField('is_mean', default=True)
    num_of_pins = BooleanField('num_of_pins', default=True)
    num_of_likes = BooleanField('num_of_likes', default=True)
    num_of_reposts = BooleanField('num_of_reposts', default=True)
    num_of_comments = BooleanField('num_of_comments', default=True)
