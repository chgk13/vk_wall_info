import app.compute as compute
import pandas as pd
from flask import render_template, flash, redirect
from app import app
from app.forms import InputForm, StatisticForm


res_df = None


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def input_form():
    form = InputForm()

    if form.is_submitted() and form.user_id.data.strip():

        params = {
            'deadline': form.from_time.data,
            'post_id': form.post_id.data,
            'text': form.text.data,
            'pins': form.pins.data,
            'num_of_pins': form.num_of_pins.data,
            'num_of_likes': form.num_of_likes.data,
            'num_of_reposts': form.num_of_reposts.data,
            'num_of_comments': form.num_of_comments.data,
            'latency': form.latency.data,
        }
        if form.user_id.data.isdigit() or form.user_id.data[1:].isdigit():
            params['user_id'] = form.user_id.data
            result = compute.compute_table((params))

            if isinstance(result, dict):
                flash(
                    f"""Error code: {result['error_code']},
                     Error message: {result['error_msg']}""")
                return redirect('/index')

            globals()['res_df'] = result
            return compute.serve_csv(result, params['user_id'])
        else:
            params['domain'] = form.user_id.data
            result = compute.compute_table((params))

            if isinstance(result, dict):
                flash(
                    f"""Error code: {result['error_code']},
                     Error message: {result['error_msg']}""")
                return redirect('/index')

            globals()['res_df'] = result
            return compute.serve_csv(result, params['domain'])

    return render_template('index.html', form=form)


@app.route('/statistic', methods=['GET', 'POST'])
def statistic():
    global res_df
    if res_df is None:
        return render_template('no_data.html')
    else:
        form = StatisticForm()
        if form.is_submitted():

            params = {
                'head': form.head.data,
                'describe_table': form.describe_table.data,
                'plot_type': form.plot_type.data,
                'time_interval': form.time_interval.data,
                'is_mean': form.is_mean.data,
                'num_of_pins': form.num_of_pins.data,
                'num_of_likes': form.num_of_likes.data,
                'num_of_reposts': form.num_of_reposts.data,
                'num_of_comments': form.num_of_comments.data
            }

            stats = compute.get_stats(res_df, params)
            return render_template('stats_results.html', stats=stats)
        return render_template('result.html', form=form)
